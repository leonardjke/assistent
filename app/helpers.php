<?php
/**
 * This file is part of the Minedonate Shop package.
 * @copyright (c) ltd Minedonate BY <http://minedonate.ru>
 * For full copyright and license information, please see
 * the install\docs\license_ru.txt file.
 * Build: {{ BUILD }}
 */

use Convertio\Convertio;

if (! function_exists('app')) {
	function app($abstract = null) {
		global $app;
		if (is_null($abstract)) {
			return $app;
		}
		return $app[$abstract];
	}
}

if (! function_exists('view')) {
	function view($view = null, $data = []) {
		return app('twig')->render($view, $data);
	}
}

function read_doc($file) {
	$API = new Convertio("821db4a5fa59798578cdf80132e32338");
	$Text = $API->start($file, 'txt')->wait()->fetchResultContent()->result_content;
	$API->delete();
	return $Text;
}

function read_docx($file){
	$striped_content = '';
	$content = '';
	$zip = zip_open($file);
	if (!$zip || is_numeric($zip)) return false;
	while ($zip_entry = zip_read($zip)) {
		if (zip_entry_open($zip, $zip_entry) == FALSE) continue;
		if (zip_entry_name($zip_entry) != "word/document.xml") continue;
		$content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
		zip_entry_close($zip_entry);
	}
	zip_close($zip);
	$content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
	$content = str_replace('</w:r></w:p>', "\r\n", $content);
	$striped_content = strip_tags($content);
	return $striped_content;
}

function read_xlsx($file){
	$xml_filename = "xl/sharedStrings.xml"; //content file name
	$zip_handle = new ZipArchive;
	$output_text = "";
	if(true === $zip_handle->open($file)){
		if(($xml_index = $zip_handle->locateName($xml_filename)) !== false){
			$xml_datas = $zip_handle->getFromIndex($xml_index);
			$xml_handle = DOMDocument::loadXML($xml_datas, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);
			$output_text = strip_tags($xml_handle->saveXML());
		}else{
			$output_text .="";
		}
		$zip_handle->close();
	}else{
		$output_text .="";
	}
	return $output_text;
}

function uploadToApi($target_file){
	$fileData = fopen($target_file, 'r');
	$client = new \GuzzleHttp\Client();
	$text = '';
	try {
		$r = $client->request('POST', 'https://api.ocr.space/parse/image',[
			'headers' => [
//				'apiKey' => 'e5a6de8b5e88957',
				'apikey' => '5a64d478-9c89-43d8-88e3-c65de9999580',
			],
			'multipart' => [
				[
					'name' => 'file',
					'contents' => $fileData,
				],
				[
					'name' => 'language',
					'contents' => 'eng',
				],
				[
					'name' => 'detectOrientation',
					'contents' => 'true',
				],
				[
					'name' => 'isSearchablePdfHideTextLayer',
					'contents' => 'true',
				],
			],
		], ['file' => $fileData,]);
		$response =  json_decode($r->getBody(),true);
	} catch(Exception $e) {
		return $text;
	}

	if($response['ErrorMessage'] == "") {
		foreach($response['ParsedResults'] as $pareValue) {
			$text = $text . $pareValue['ParsedText'];
		}
	}
	return $text;
}

function searchUnpRegexp($text) {
	preg_match_all('/[0-9]{9}/', $text, $response);
	return $response[0][0];
}
function searchAccRegexp(&$text) {
	preg_match('/[a-zA-Z]{2}[0-9]{2} ?[a-zA-Z]{4} ?([0-9]{4} ?){5}/', $text, $response);
	// preg_match('/[a-zA-Z]{2}[0-9]{2}[a-zA-Z]{4}[0-9]{20}/', $text, $response);
	$response[0] = str_replace(' ', '', $response[0]);
	$text = str_ireplace($response[0], '', $text);
	return $response[0];
}
function searchSumRegexp($text) {
	preg_match_all('/([0-9]{1,3} ?)?([0-9]{1,3} ?)?[0-9]{1,3}[,.][0-9]{2}/', $text, $response);
	return $response[0][count($response[0]) - 1];
}
function searchAccountData($text) {

	$textResponse = $text;
	$sumAccount = searchSumRegexp($text);
	$bankAccount = searchAccRegexp($text);
	$unpAccount = searchUnpRegexp($text);

	return [
		'sum' => $sumAccount ?? '',
		'acc' => $bankAccount ?? '',
		'unp' => $unpAccount ?? '',
//		'txt' => $textResponse, // debug
	];

}