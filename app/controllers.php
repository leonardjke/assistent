<?php

use Symfony\Component\HttpFoundation\Request;

$app->get('/', function () use ($app) {
	$errors = [];
	return view('index.twig', compact('errors'));
});

$app->post('/upload', function (Request $request) use ($app) {

	$file = $request->files->get('file');
	$info = new SplFileInfo($file->getClientOriginalName());
	$filename = sprintf('%d.%s', time(), $info->getExtension());
	$mimeType = $file->getMimeType();
	$file->move(__DIR__ . '/../public/uploads/' , $filename);
	$path = __DIR__ . '/../public/uploads/' . $filename;

	$app['monolog']->info($mimeType);

	switch ($mimeType) {
		case 'image/jpeg':
		case 'image/jpg':
		case 'image/png':
		case 'application/pdf':
			$text = uploadToApi($path);
			break;
		case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
			$text = read_docx($path);
			break;
		case 'application/msword':
			$text = read_doc($path);
			break;
		case 'application/octet-stream':
			$text = read_xlsx($path);
			break;
		default:
			return $app->json([
				'message' => 'Данный формат файла не поддерживается',
			]);
	}

	//	if(unlink(__DIR__ . '/../public/uploads/' . $filename)){
		return $app->json(searchAccountData($text));
	//	}
	//	return $app->json(searchAccountData($text) + ['message' => 'Файл не удален']);

});