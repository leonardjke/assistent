<?php
/**
 * This file is part of the Minedonate Shop package.
 * @copyright (c) ltd Minedonate BY <http://minedonate.ru>
 * For full copyright and license information, please see
 * the install\docs\license_ru.txt file.
 * Build: {{ BUILD }}
 */


use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());

$app['twig'] = $app->extend('twig', function ($twig, $app) {
	return $twig;
});

return $app;
