<?php
/**
 * This file is part of the Minedonate Shop package.
 * @copyright (c) ltd Minedonate BY <http://minedonate.ru>
 * For full copyright and license information, please see
 * the install\docs\license_ru.txt file.
 * Build: {{ BUILD }}
 */

define('MINEDONATE_START', microtime(true));


/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/

require __DIR__.'/../vendor/autoload.php';
require __DIR__ . '/../app/app.php';
require __DIR__.'/../config/app.php';
require __DIR__.'/../app/controllers.php';



$app->run();