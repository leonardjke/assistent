window.addEventListener('load', function() {

    function setAlert(message) {
        let a = $('.dialog').append('<div class="alert alert-danger" role="alert">' + message + '</div>');
        setTimeout(function () {
            a.detach();
        },10000);
    }

    function setInvalidInput(input) {
        $(input).addClass('is-invalid');
        $(input).parent().append('<span class="invalid-feedback" role="alert"><strong>Упс, не получилось распарсить</strong></span>');
        setTimeout(function () {
            $(input).parent().find('.invalid-feedback').detach();
        }, 3000)
    }
    function setUninvalidInput(input) {
        $(input).removeClass('is-invalid');
        $(input).removeClass('is-valid');
    }
    function setValidInput(input) {
        $(input).addClass('is-valid');
    }
    function setValueToInput(input, value) {
        $(input).val(value);
    }
    function clearAllInput() {
        setUninvalidInput('#acc');
        setValueToInput('#acc', '');
        setUninvalidInput('#sum');
        setValueToInput('#sum', '');
        setUninvalidInput('#unp');
        setValueToInput('#unp', '');
    }

    $("#drop-area").dmUploader({
        url: '/upload',
        maxFileSize: 3000000, // 3 Megs

        onInit: function(){
            console.log('Callback: Plugin initialized');
        },
        onNewFile: function(id, file){
            // When a new file is added using the file selector or the DnD area
            console.log('New file added #' + id);
            clearAllInput()
        },
        onBeforeUpload: function(id){
            console.log('Starting the upload of #' + id);
            $(this).find('#button')[0].classList.add("btn-loading");
        },
        onUploadSuccess: function(id, data){
            console.log('Server Response for file #' + id + ': ' + JSON.stringify(data));
            console.log('Upload of file #' + id + ' COMPLETED', 'success');
            $(this).find('#button')[0].classList.remove("btn-loading");

            var e = jQuery.parseJSON(JSON.stringify(data));
            if(e.message) {
                setAlert(e.message);
            } else {
                if(e.acc === ''){
                    setInvalidInput("#acc");
                } else {
                    setValidInput('#acc');
                    setValueToInput('#acc', e.acc);
                }
                if(e.sum === ''){
                    setInvalidInput("#sum");
                } else {
                    setValidInput('#sum');
                    setValueToInput('#sum', e.sum);
                }
                if(e.unp === ''){
                    setInvalidInput("#unp");
                } else {
                    setValidInput('#unp');
                    setValueToInput('#unp', e.unp);
                }
            }

        },
        onComplete: function(){
            // All files in the queue are processed (success or error)
            console.log('All pending tranfers finished');
        },
        onUploadError: function(id, xhr, status, message){
            $(this).find('#button')[0].classList.remove("btn-loading");
        },


        onDragEnter: function(){
            this.addClass('active');
        },
        onDragLeave: function(){
            this.removeClass('active');
        },

        onComplete: function(){
            // All files in the queue are processed (success or error)
            console.log('All pending tranfers finished');
        },
        onFallbackMode: function(){
            // When the browser doesn't support this plugin :(
            console.log('Plugin cant be used here, running Fallback callback', 'danger');
        },
        onFileSizeError: function(file){
            console.log('File \'' + file.name + '\' cannot be added: size excess limit', 'danger');
            setAlert('Файл слишком большой (' + file.size + ')');
        }

        // ... More callbacks
    });
});