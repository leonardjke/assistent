<?php
/**
 * This file is part of the Minedonate Shop package.
 * @copyright (c) ltd Minedonate BY <http://minedonate.ru>
 * For full copyright and license information, please see
 * the install\docs\license_ru.txt file.
 * Build: {{ BUILD }}
 */



// enable the debug mode
use Silex\Provider\MonologServiceProvider;

$app['debug'] = true;

$app['twig.path'] = array(__DIR__.'/../templates');

if($app['debug']) {
	$app->register(new MonologServiceProvider(), array(
		'monolog.logfile' => __DIR__.'/../storage/logs/file.log',
	));
} else {
	$app['twig.options'] = array('cache' => __DIR__.'/../storage/cache/twig');
}

require __DIR__.'/../app/helpers.php';