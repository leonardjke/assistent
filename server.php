<?php
/**
 * This file is part of the Minedonate Shop package.
 * @copyright (c) ltd Minedonate BY <http://minedonate.ru>
 * For full copyright and license information, please see
 * the install\docs\license_ru.txt file.
 * Build: {{ BUILD }}
 */

$uri = urldecode(
	parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
);

if ($uri !== '/' && file_exists(__DIR__.'/public'.$uri)) {
	return false;
}

require_once __DIR__.'/public/index.php';